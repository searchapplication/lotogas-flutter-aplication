# LotoGás
Repositório do aplicativo LotoGás desenvolvido com Flutter.
<br/>
---
### Instalação
1. Defina um local e faça o **Clone** do repositório.
2. Acesse a pasta **lotogas-flutter-aplication/lotogas**.
3. Abra o terminal e execute **flutter doctor**.
4. Com as pendências resolvidas, abra o emulador Android/iOS ou conecte o smartphone via USB *(com modo desenvolvedor ativo)*. 
5. Execute o comando **flutter devices** e confirme se o aparelho/emulador está conectado.
6. Execute **flutter run**.


---
### Recomendação IDE
Utilizar o editor **Visual Studio Code** e com as seguintes extensões instaladas:

1. Dart
2. Flutter
3. Material Icon Theme
4. Todo Tree


---
### Documentação
Abaixo estão alguns links para documentações do projeto:

1. Layout de telas: https://app.zeplin.io/project/5d483a66f95862313a29dd6b/dashboard
2. Escopo do projeto: https://diastecnologia.atlassian.net/wiki/spaces/MARKETPLAC/pages/262221/Escopo+Aplicativo+LotoG+s