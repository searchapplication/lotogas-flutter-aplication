import 'package:flutter/material.dart';

class AppBarDefault extends StatelessWidget implements PreferredSizeWidget {
  AppBarDefault({@required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: TextStyle(
          color: Color(0xff020202),
          fontWeight: FontWeight.w400,
        ),
      ),
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Color(0xff0000cc)),
      elevation: 0,
      bottom: PreferredSize(
          child: Container(
            color: Color.fromARGB(255, 229, 229, 229),
            height: 1,
          ),
          preferredSize: Size.fromHeight(1)),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
