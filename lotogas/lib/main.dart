import 'package:flutter/material.dart';
import 'package:lotogas/screens/HomePage.dart';
import 'package:lotogas/screens/Login.dart';
import 'package:lotogas/screens/LotteryPage.dart';
import 'package:lotogas/screens/MyOrdersPage.dart';
import 'package:lotogas/screens/ShoppingCartPage.dart';
import 'package:lotogas/theme/style.dart';
import 'screens/StoreHomePage.dart';
import 'screens/AccountInformationPage.dart';

void main() {
  runApp(LotoGas());
}

class LotoGas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LotoGás',
      theme: appTheme(),
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) => Login(),
        "/home": (BuildContext context) => HomePage(),
        "/storeHome": (BuildContext context) => StoreHomePage(),
        "/accountInformation": (BuildContext context) =>
            AccountInformationPage(),
        "/lotteryPage": (BuildContext context) => LotteryPage(),
        "/myOrdersPage": (BuildContext context) => MyOrdersPage(),
        "/paymentMethod": (BuildContext context) => PaymentMethod(),
        "/ShoppingCartPage":(BuildContext context) => ShoppingCartPage(),
      },
    );
  }
}
