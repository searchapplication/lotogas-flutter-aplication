import 'package:flutter/material.dart';

class AccountInformationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(55),
        child: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.white,
          title: Row(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                right: 30.0,
              )),
              Text(
                "Informações da Conta",
                style: TextStyle(
                  fontFamily: 'Roboto Light',
                  fontSize: 16,
                  color: HexColor("#000000"),
                ),
              ),
            ],
          ),
          leading: IconButton(
            focusColor: Colors.transparent,
            icon: Image.asset(
              "assets/icons/3.0x/icone_voltar.png",
              width: 20,
              height: 20,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
      ),
      body: ListView(physics: const NeverScrollableScrollPhysics(),
//        padding: EdgeInsets.only(left: 32.0, top: 16.0),
          children: <Widget>[
            Container(
              height: 96,
              alignment: Alignment.center,
              child: ListTile(
                leading: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/icons/foto.png'),
                    ),
                  ),
                  child: new FlatButton(
                      textColor: Colors.white,
                      onPressed: () {
                        print("Foto perfil");
                      }),
                ),
                title: Text(
                  "Camila Alves",
                  style: TextStyle(
                      fontFamily: 'Roboto Regular',
                      fontSize: 18,
                      color: HexColor("#000000")),
                ),
                subtitle: Text(
                  "camilaalves@gmail.com",
                  style: TextStyle(
                    fontFamily: 'Roboto Regular',
                    fontSize: 16,
                    color: HexColor("#999999"),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 16.0,
              child: Container(
                color: HexColor("#e2e2e2"),
              ),
            ),
            DrawerTile(
              Image.asset(
                "assets/icons/3.0x/user.png",
                height: 20,
                width: 20,
              ),
              "Minhas informações",
              0,
            ),
            DrawerTile(
              Image.asset(
                "assets/icons/3.0x/place_localizer.png",
                height: 20,
                width: 20,
              ),
              "Meus endereços",
              1,
            ),
            DrawerTile(
              Image.asset(
                "assets/icons/3.0x/shopping_cart_6.png",
                height: 20,
                width: 20,
              ),
              "Meus pedidos",
              2,
            ),
            DrawerTile(
              Image.asset(
                "assets/icons/3.0x/console.png",
                height: 20,
                width: 20,
              ),
              "Meus jogos",
              3,
            ),
            DrawerTile(
              Image.asset(
                "assets/icons/3.0x/logout.png",
                height: 20,
                width: 20,
              ),
              "Sair",
              4,
            ),
          ]),
    );
  }
}

class DrawerTile extends StatelessWidget {
  final Image icon;
  final String text;
  final int page;

  DrawerTile(this.icon, this.text, this.page);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 64,
          child: Material(
            color: Colors.white,
            child: InkWell(
              onTap: () {
                print(text);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: 60.0,
                    child: Row(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                          left: 25,
                        )),
                        this.icon,
                        Padding(
                            padding: EdgeInsets.only(
                          left: 15,
                        )),
                        Text(
                          text,
                          style: TextStyle(
                            fontFamily: 'Roboto Regular',
                            fontSize: 18.0,
                            color: HexColor("#000000"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Divider(
          height: 1,
          color: HexColor("#e2e2e2"),
        ),
      ],
    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
