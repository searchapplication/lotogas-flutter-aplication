import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String profilePhotoUrl =
      "https://cdn.zeplin.io/5d483a66f95862313a29dd6b/assets/7bb14d31-cd84-43fe-9938-c76d1a499064.png";

  TextEditingController _textFieldController = TextEditingController();

  String dropdownValue = "R. Hilda Bergo Duarte, 842";

  @override
  Widget build(BuildContext context) {
    final items = List<String>.generate(10000, (i) => "$i");

    void _showDialog() {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              width: double.maxFinite,
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      "Ordenar por",
                      style: TextStyle(
                        color: Color(0xff999999),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Menor distância"),
                          Icon(
                            Icons.check_circle,
                            color: Color(0xff0000cc),
                          )
                        ]),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Menor preço"),
                          Icon(
                            Icons.radio_button_unchecked,
                            color: Color(0xffe2e2e2),
                          )
                        ]),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Melhor avaliação"),
                          Icon(
                            Icons.radio_button_unchecked,
                            color: Color(0xffe2e2e2),
                          )
                        ]),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 15.0,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Expanded(
                          flex: 1,
                          child: OutlineButton(
                            color: Color(0xff0000cc),
                            textColor: Color(0xff0000cc),
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                color: Color(0xffeeeeee),
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(5.0),
                              ),
                            ),
                            child: new Text("Cancelar"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        SizedBox(
                          width: 20.0,
                        ),
                        Expanded(
                          flex: 1,
                          child: FlatButton(
                            color: Color(0xff0000cc),
                            textColor: Colors.white,
                            child: new Text("Ordenar"),
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                color: Color(0xff0000cc),
                                width: 1.0,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(5.0),
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                left: 15.0,
                right: 15.0,
                top: 15.0,
                bottom: 15.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 5,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "ENTREGAR EM",
                          style: TextStyle(
                            fontSize: 16,
                            color: Color(0xff999999),
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: dropdownValue,
                            isExpanded: true,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                dropdownValue = newValue;
                              });
                            },
                            iconEnabledColor: Color(0xff0000cc),
                            icon: Icon(Icons.keyboard_arrow_down),
                            items: <String>[
                              "R. Hilda Bergo Duarte, 842",
                              "Av. Marcelino Pires da Silva Costa, 2332",
                              "R. João Damasceno Pires, 1190"
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 2,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/accountInformation');
                      },
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(100.0),
                        child: Image.network(
                          profilePhotoUrl,
                          height: 45.0,
                          width: 45.0,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: 15.0,
                right: 15.0,
                bottom: 10.0,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 5,
                    child: Container(
                      margin: EdgeInsets.only(right: 15.0),
                      color: Color(0xffe2e2e2),
                      height: 35,
                      child: TextField(
                        controller: _textFieldController,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w300,
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(top: 7),
                          focusColor: Colors.red,
                          hintText: "Estabelecimento",
                          hintStyle: TextStyle(color: Color(0xff999999)),
                          prefixIcon: Icon(
                            Icons.search,
                            size: 28.0,
                            color: Color(0xff0000cc),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        _showDialog();
                      },
                      child: Text("Filtros",
                          style: TextStyle(
                              fontSize: 18, color: Color(0xff0000cc))),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: Color(0xffcccccc),
            ),
            Expanded(
              child: Container(
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        leading: Image.network(
                            "https://media-cdn.tripadvisor.com/media/photo-s/0d/0a/6f/d2/frente-de-loja.jpg"),
                        title: Text(
                          'Copagaz ${items[index]}',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Rua Major Capilé - 0,4 km',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            Text(
                              '★ 5.0',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Color(0xfff2bb18),
                              ),
                            )
                          ],
                        ),
                        isThreeLine: true,
                        onTap: () => Navigator.pushNamed(context, '/storeHome'),
                      ),
                      margin: EdgeInsets.only(
                        top: 8.0,
                        left: 15.0,
                        right: 15.0,
                        bottom: 0.0,
                      ),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: Color(0xffeeeeee),
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
