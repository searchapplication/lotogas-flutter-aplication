import 'package:flutter/material.dart';
import 'package:lotogas/screens/HomePage.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  void _handleCadastreSe() {
    print('Cadastre-se');
  }

  void _handleRecuperarSenha() {
    print('Recuperar senha');
  }

  void _handleEntrar() {
    print('Login: ${_loginController.text}');
    print('Senha: ${_passwordController.text}');
    print('Entrar');
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => HomePage()));
  }

  void _handleLoginFacebook() {
    print('Iniciar sessão no facebook');
  }

  void _handlerLoginGmail() {
    print('Iniciar sessão no gmail');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 72, 0, 61),
              child: Image.asset('assets/icons/logo.png'),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 27),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Login',
                    style: TextStyle(
                      fontFamily: 'Roboto-Regular',
                      fontSize: 16.0,
                      color: Color.fromRGBO(0, 0, 204, 1),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextField(
                    controller: _loginController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 15, 0, 14),
                      hintText: 'E-mail',
                      hintStyle: TextStyle(
                        color: Color.fromRGBO(204, 204, 204, 1),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 0, 204, 1),
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 0, 204, 1),
                          width: 1.0,
                          style: BorderStyle.solid,
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(204, 0, 0, 1),
                        ),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(204, 204, 204, 1),
                        ),
                      ),
                    ),
                    style: TextStyle(
                      fontFamily: 'Roboto-Regular',
                      fontSize: 16.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 11),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Senha',
                    style: TextStyle(
                      fontFamily: 'Roboto-Regular',
                      fontSize: 16.0,
                      color: Color.fromRGBO(0, 0, 204, 1),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 15, 0, 14),
                      hintText: 'Digite aqui sua senha',
                      hintStyle: TextStyle(
                        color: Color.fromRGBO(204, 204, 204, 1),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 0, 204, 1),
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(0, 0, 204, 1),
                          width: 1.0,
                          style: BorderStyle.solid,
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(5),
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(204, 0, 0, 1),
                        ),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color.fromRGBO(204, 204, 204, 1),
                        ),
                      ),
                    ),
                    style: TextStyle(
                      fontFamily: 'Roboto-Regular',
                      fontSize: 16.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    child: Text(
                      'Cadastre-se',
                      style: TextStyle(
                        fontFamily: 'Roboto-Regular',
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                    ),
                    onTap: _handleCadastreSe,
                  ),
                  GestureDetector(
                    child: Text(
                      'Recuperar senha',
                      style: TextStyle(
                        fontFamily: 'Roboto-Regular',
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                    ),
                    onTap: _handleRecuperarSenha,
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  MaterialButton(
                    elevation: 0,
                    height: 48,
                    color: Color.fromRGBO(0, 0, 204, 1),
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: Container(
                      child: Text(
                        'ENTRAR',
                        style: TextStyle(
                            fontFamily: 'Roboto-Regular',
                            fontSize: 16.0,
                            color: Colors.white),
                      ),
                    ),
                    onPressed: _handleEntrar,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                vertical: 23,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Color.fromRGBO(204, 204, 204, 1),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    'ou',
                    style: TextStyle(
                      fontFamily: 'Roboto-Regular',
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: Container(
                      height: 1,
                      color: Color.fromRGBO(204, 204, 204, 1),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: OutlineButton(
                onPressed: _handleLoginFacebook,
                padding: EdgeInsets.symmetric(
                  vertical: 14,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/icons/facebook_logo.png'),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Iniciar sessão no facebook',
                      style: TextStyle(
                        fontFamily: 'Roboto-Regular',
                        fontSize: 16.0,
                        color: Color.fromRGBO(153, 153, 153, 1),
                      ),
                    )
                  ],
                ),
                borderSide: BorderSide(
                  color: Color.fromRGBO(153, 153, 153, 1),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
              ),
              margin: EdgeInsets.only(bottom: 16),
            ),
            Container(
              child: OutlineButton(
                onPressed: _handlerLoginGmail,
                padding: EdgeInsets.symmetric(
                  vertical: 14,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/icons/gmail_logo.png'),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Iniciar sessão no gmail',
                      style: TextStyle(
                        fontFamily: 'Roboto-Regular',
                        fontSize: 16.0,
                        color: Color.fromRGBO(153, 153, 153, 1),
                      ),
                    )
                  ],
                ),
                borderSide: BorderSide(
                  color: Color.fromRGBO(153, 153, 153, 1),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                ),
              ),
              margin: EdgeInsets.only(bottom: 40),
            ),
          ],
        ),
      ),
    );
  }
}
