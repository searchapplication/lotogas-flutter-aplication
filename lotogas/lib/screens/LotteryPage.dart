import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lotogas/components/AppBarDefault.dart';

class LotteryPage extends StatefulWidget {
  @override
  _LotteryPageState createState() => _LotteryPageState();
}

class _LotteryPageState extends State<LotteryPage> {
  var selectedNumbers = [];
  String dropdownValue = null;

  void _checkSelected(int valor) {
    var newSelectedNumbers = []..addAll(selectedNumbers);
    if (newSelectedNumbers.contains(valor)) {
      newSelectedNumbers.remove(valor);
    } else {
      newSelectedNumbers.add(valor);
    }
    print(newSelectedNumbers);
    setState(() {
      selectedNumbers = newSelectedNumbers;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBarDefault(title: "Prêmio"),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 15.0,
              bottom: 15.0,
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Parabéns pela compra! Você ganhou um jogo grátis. Escolha qual forma quer jogar.",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              bottom: 5.0,
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Escolha qual forma deseja jogar!",
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xffafafaf),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 50.0,
              right: 50.0,
            ),
            padding: EdgeInsets.only(
              left: 10.0,
              right: 10.0,
              top: 5.0,
              bottom: 5.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Color(0xffafafaf),
                  style: BorderStyle.solid,
                  width: 0.80),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                value: dropdownValue,
                isExpanded: true,
                isDense: true,
                hint: new Text("Escolha qual opção deseja jogar"),
                onChanged: (String newValue) {
                  setState(() {
                    if (newValue == "Automaticamente") {
                      // Gerar números aleatórios via API
                      selectedNumbers = [
                        Random().nextInt(10),
                        Random().nextInt(20),
                        Random().nextInt(30),
                        Random().nextInt(40),
                        Random().nextInt(50),
                        Random().nextInt(60),
                      ];
                    } else {
                      selectedNumbers = [];
                    }
                    dropdownValue = newValue;
                  });
                },
                iconEnabledColor: Color(0xff0000cc),
                icon: Icon(Icons.keyboard_arrow_down),
                items: <String>["Manualmente", "Automaticamente"]
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(
                      value,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 20.0,
            ),
            child: Align(
              child: Container(
                child: Text(
                  "Mega-Sena",
                  style: TextStyle(
                    color: Color(0xff0000cc),
                    fontSize: 18.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                padding: EdgeInsets.all(5.0),
                decoration: new BoxDecoration(
                  color: Color(0xffcccccc),
                  borderRadius: new BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
              ),
              alignment: Alignment.centerLeft,
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 5.0,
            ),
            child: Align(
              child: Text(
                "sorteio: 22/10",
                style: TextStyle(
                  fontSize: 16.0,
                ),
              ),
              alignment: Alignment.centerLeft,
            ),
          ),
          Container(
            height: 250.0,
            margin: EdgeInsets.all(15.0).add(
              EdgeInsets.only(top: 0),
            ),
            child: GridView.count(
              crossAxisCount: 10,
              shrinkWrap: true,
              children: List.generate(60, (index) {
                index++;
                return Container(
                  margin: EdgeInsets.all(4.0),
                  child: FlatButton(
                    padding: EdgeInsets.all(0.0),
                    color: selectedNumbers.contains(index) == true
                        ? Color(0xff0000cc)
                        : Color(0xffcccccc),
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        color: selectedNumbers.contains(index) == true
                            ? Color(0xff0000cc)
                            : Color(0xffcccccc),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.all(
                        Radius.circular(7.0),
                      ),
                    ),
                    child: Text(
                      '$index',
                      style: TextStyle(
                        color: selectedNumbers.contains(index) == true
                            ? Color(0xffffffff)
                            : Color(0xff0000cc),
                        fontWeight: FontWeight.w500,
                        fontSize: 17,
                      ),
                    ),
                    onPressed: () => _checkSelected(index),
                  ),
                );
              }),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 5.0,
            ),
            child: Align(
              child: FlatButton(
                padding: EdgeInsets.only(
                  left: 35.0,
                  right: 35.0,
                ),
                color: Color(0xff0000cc),
                child: Text(
                  "Confirmar",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                ),
                onPressed: () {},
              ),
              alignment: Alignment.center,
            ),
          ),
          // CircularProgressIndicator(
          //   valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff0000cc)),
          // ),
        ],
      ),
    );
  }
}
