import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:lotogas/components/AppBarDefault.dart';

class MyOrdersPage extends StatefulWidget {
  @override
  _MyOrdersPageState createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  bool loading = true;
  List<MonthOrders> _monthOrdersList = [];

  @override
  initState() {
    super.initState();
    initializeDateFormatting('pt_BR');
    _getMyOrders();
  }

  void _getMyOrders() {
    getFromAPI().then((list) {
      setState(() {
        loading = false;
        _monthOrdersList = list;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarDefault(title: 'Meus Pedidos'),
        body: MonthOrdersListWidget(
            loading: loading, monthOrdersList: _monthOrdersList));
  }
}

class MonthOrdersListWidget extends StatelessWidget {
  MonthOrdersListWidget(
      {Key key, @required this.loading, @required this.monthOrdersList})
      : super(key: key);

  final bool loading;
  final List<MonthOrders> monthOrdersList;

// TODO loading indicator
  Widget _display() {
    return ListView.builder(
        itemCount: monthOrdersList.length,
        itemBuilder: (BuildContext context, int index) {
          return MonthOrdersCardWidget(
              index: index, monthOrders: monthOrdersList[index]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(16, 18, 16, 12),
            child: Text(
              'Histórico de pedidos ',
              style: TextStyle(
                fontFamily: 'Roboto-Regular',
                fontSize: 18.0,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            height: 8,
            color: Color.fromRGBO(226, 226, 226, 1),
          ),
          Flexible(
            fit: FlexFit.tight,
            child: _display(),
          ),
        ],
      ),
    );
  }
}

class MonthOrdersCardWidget extends StatelessWidget {
  MonthOrdersCardWidget(
      {Key key, @required this.index, @required this.monthOrders})
      : super(key: key);

  final int index;
  final MonthOrders monthOrders;

  Widget _displayCard() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: Text(
                  '${monthOrders.month}',
                  style: TextStyle(
                    fontFamily: 'Roboto-Regular',
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ),
              OrderListWidget(orders: monthOrders.orders),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return index > 0
        ? Column(
            children: <Widget>[
              Container(
                height: 8,
                color: Color.fromRGBO(226, 226, 226, 1),
              ),
              _displayCard()
            ],
          )
        : _displayCard();
  }
}

class OrderListWidget extends StatelessWidget {
  OrderListWidget({Key key, @required this.orders}) : super(key: key);

  final List<Order> orders;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: orders.map((order) => OrderCardWidget(order: order)).toList(),
    );
  }
}

class OrderCardWidget extends StatelessWidget {
  OrderCardWidget({Key key, @required this.order}) : super(key: key);

  final Order order;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      decoration: BoxDecoration(
        color: Color(0xffffffff).withOpacity(1),
        borderRadius: BorderRadius.circular(3),
        boxShadow: [
          BoxShadow(
            color: Color(0xff0000cc),
            offset: Offset(0, -2),
          ),
          BoxShadow(
            color: Color(0xffcccccc),
            offset: Offset(1, 1),
          ),
          BoxShadow(
            color: Color(0xffcccccc),
            offset: Offset(-1, 1),
          ),
        ],
      ),
      padding: EdgeInsets.all(14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 6),
                  child: Image.asset(
                    'assets/icons/gas_2.png',
                    width: 15,
                    height: 24,
                  ),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 6),
                          child: Row(
                            children: <Widget>[
                              Text(
                                'Código',
                                style: TextStyle(
                                  fontFamily: 'Roboto-Regular',
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                              ),
                              Text(
                                ' : ${order.orderCode}',
                                style: TextStyle(
                                  fontFamily: 'Roboto-Light',
                                  fontSize: 16,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Text(
                            'Realizado em ${order.getOrderDateFormated()}',
                            style: TextStyle(
                              fontFamily: 'Roboto-Light',
                              fontSize: 14,
                              color: Color(0xff999999),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Color(0xffcccccc),
          ),
          OrderItemListWidget(orderItems: order.list),
          Divider(
            color: Color(0xffcccccc),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  'Valor ${order.getTotalFormated()}',
                  style: TextStyle(
                    fontFamily: 'Roboto-Regular',
                    fontSize: 16,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class OrderItemListWidget extends StatelessWidget {
  OrderItemListWidget({Key key, @required this.orderItems}) : super(key: key);

  final List<OrderItem> orderItems;

  TableRow _tableHeaders() {
    return TableRow(
      children: [
        Text(
          'Qtds',
          style: TextStyle(
            fontFamily: 'Roboto-Regular',
            fontSize: 16,
            color: Colors.black,
          ),
        ),
        Text(
          'Itens',
          style: TextStyle(
            fontFamily: 'Roboto-Regular',
            fontSize: 16,
            color: Colors.black,
          ),
        ),
        Text(
          'Preços',
          style: TextStyle(
            fontFamily: 'Roboto-Regular',
            fontSize: 16,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  List<TableRow> _tableDatas() {
    return orderItems
        .map(
          (orderItem) => TableRow(
            children: [
              Center(
                child: Text(
                  '${orderItem.quantity}',
                  style: TextStyle(
                    fontFamily: 'Roboto-Light',
                    fontSize: 14,
                    color: Colors.black,
                  ),
                ),
              ),
              Text(
                '${orderItem.name}',
                style: TextStyle(
                  fontFamily: 'Roboto-Light',
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
              Text(
                '${orderItem.getPriceFormated()}',
                style: TextStyle(
                  fontFamily: 'Roboto-Light',
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        )
        .toList();
  }

  List<TableRow> _showTable() {
    List<TableRow> result = [];
    result.add(_tableHeaders());

    for (var data in _tableDatas()) {
      result.add(data);
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Table(
            columnWidths: {
              0: FixedColumnWidth(50),
              1: FlexColumnWidth(1),
              2: IntrinsicColumnWidth(),
            },
            children: _showTable(),
          ),
        ],
      ),
    );
  }
}

Future<List<MonthOrders>> getFromAPI() async {
  MonthOrdersList monthOrdersList = MonthOrdersList();

  for (var i = 0; i < 10000; i++) {
    Order order = Order((345764 - i).toString(),
        DateTime.now().subtract(Duration(days: i * 30)));
    order.add(OrderItem('Garrafão de água mineiral', 1, 1 * 11.00));
    order.add(OrderItem('Gás de cozinha', 2, 2 * 71.00));

    monthOrdersList.add(order);
  }

  return monthOrdersList.list;
}

class MonthOrdersList {
  final List<MonthOrders> list = [];

  void add(Order order) {
    int year = DateTime.now().year;
    int month = DateTime.now().month;

    String monthLabel = 'Mês Atual';

    if (order.orderDate.year != year || order.orderDate.month != month) {
      String monthOfYear = DateFormat.yMMMM("pt").format(order.orderDate);
      monthLabel = toBeginningOfSentenceCase(monthOfYear);
    }

    MonthOrders monthOrders = list.firstWhere(
        (monthOrder) => monthOrder.month == monthLabel,
        orElse: () => null);

    if (monthOrders == null) {
      monthOrders = MonthOrders(monthLabel, []);
      list.add(monthOrders);
    }
    monthOrders.add(order);
  }
}

class MonthOrders {
  final String month;
  final List<Order> orders;

  MonthOrders(this.month, this.orders);

  void add(Order order) {
    orders.add(order);
  }
}

class Order {
  final String orderCode;
  final DateTime orderDate;
  double _total = 0;

  final List<OrderItem> list = [];

  Order(this.orderCode, this.orderDate);

  void add(OrderItem orderItem) {
    _total += orderItem.price;
    list.add(orderItem);
  }

  String getOrderDateFormated() {
    return DateFormat('dd/MM/yyyy', 'pt').format(orderDate);
  }

  String getTotalFormated() {
    return NumberFormat.currency(
            locale: "pt_BR", symbol: "R\$", decimalDigits: 2)
        .format(_total);
  }
}

class OrderItem {
  final String name;
  final int quantity;
  final double price;

  OrderItem(this.name, this.quantity, this.price);

  String getPriceFormated() {
    return NumberFormat.currency(
            locale: "pt_BR", symbol: "R\$", decimalDigits: 2)
        .format(price);
  }
}
