import 'dart:math';

import 'package:flutter/material.dart';
import 'package:lotogas/components/AppBarDefault.dart';

class PaymentMethod extends StatefulWidget {
  @override
  _PaymentMethodState createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  var selectedNumbers = [];
  String dropdownValue = null;
  String cardNumber = "";

  void _checkSelected(int valor) {
    var newSelectedNumbers = []..addAll(selectedNumbers);
    if (newSelectedNumbers.contains(valor)) {
      newSelectedNumbers.remove(valor);
    } else {
      newSelectedNumbers.add(valor);
    }
    print(newSelectedNumbers);
    setState(() {
      selectedNumbers = newSelectedNumbers;
    });
  }

  InputDecoration inputBorderStyle() {
    return InputDecoration(
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xff0000cc), width: 1.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xffee6cccccc), width: 1.0),
      ),
      contentPadding: EdgeInsets.all(10.0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBarDefault(title: "Pagamento"),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 15.0,
              bottom: 15.0,
            ),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Método de pagamento",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
            ),
            padding: EdgeInsets.only(
              left: 10.0,
              right: 10.0,
              top: 5.0,
              bottom: 5.0,
            ),
            height: 50.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Color(0xffafafaf),
                  style: BorderStyle.solid,
                  width: 0.80),
            ),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                value: dropdownValue,
                isExpanded: true,
                isDense: true,
                hint: new Text("Selecione o método de pagamento"),
                onChanged: (String newValue) {
                  setState(() {
                    if (newValue == "Automaticamente") {
                      // Gerar números aleatórios via API
                      selectedNumbers = [
                        Random().nextInt(10),
                        Random().nextInt(20),
                        Random().nextInt(30),
                        Random().nextInt(40),
                        Random().nextInt(50),
                        Random().nextInt(60),
                      ];
                    } else {
                      selectedNumbers = [];
                    }
                    dropdownValue = newValue;
                  });
                },
                iconEnabledColor: Color(0xff0000cc),
                icon: Icon(Icons.keyboard_arrow_down),
                items: <String>["Em dinheiro", "Cartão"]
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(
                      value,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              border: new Border.all(
                width: 1,
                color: Color(0xffcccccc),
              ),
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: DefaultTabController(
              length: 2,
              initialIndex: 1,
              child: Column(
                children: [
                  TabBar(
                    tabs: [
                      Tab(text: 'Débito'),
                      Tab(text: 'Crédito'),
                    ],
                    labelColor: Colors.black,
                    indicatorColor: Color(0xff0000cc),
                    labelStyle: TextStyle(fontSize: 18),
                  ),
                  Container(
                    height: 570.0,
                    child: TabBarView(
                      children: [
                        Center(child: Text('Pagamento em débito')),
                        Center(
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 20),
                              Stack(
                                children: <Widget>[
                                  Image.network(
                                    'https://cdn.zeplin.io/5d483a66f95862313a29dd6b/assets/20771a54-47b5-4246-8d86-e437a5a4836d.png',
                                    height: 170,
                                  ),
                                  Positioned(
                                    child: Text(
                                      cardNumber,
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                    left: 20,
                                    bottom: 60,
                                  ),
                                  Positioned(
                                    child: Text(
                                      "1234",
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Color(0xff6e6e6e),
                                      ),
                                    ),
                                    left: 20,
                                    bottom: 30,
                                  ),
                                  Positioned(
                                    child: Text(
                                      "Month/Year",
                                      style: TextStyle(
                                        fontSize: 10,
                                      ),
                                    ),
                                    left: 170,
                                    bottom: 35,
                                  ),
                                  Positioned(
                                    child: Text(
                                      "16/10",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                    left: 170,
                                    bottom: 13,
                                  ),
                                ],
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(
                                  top: 15.0,
                                  left: 15.0,
                                  right: 15.0,
                                ),
                                child: Text(
                                  "Número do cartão",
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: 5.0,
                                  left: 15.0,
                                  right: 15.0,
                                  bottom: 0.0,
                                ),
                                child: TextField(
                                  onChanged: (text) {
                                    setState(() {
                                      cardNumber = text;
                                    });
                                  },
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  decoration: inputBorderStyle(),
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(
                                  top: 10.0,
                                  left: 15.0,
                                  right: 15.0,
                                ),
                                child: Text(
                                  "Nome do Titular",
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(
                                  top: 5.0,
                                  left: 15.0,
                                  right: 15.0,
                                  bottom: 0.0,
                                ),
                                child: TextField(
                                  controller: new TextEditingController(),
                                  keyboardType: TextInputType.number,
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  decoration: inputBorderStyle(),
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 10.0,
                                          left: 15.0,
                                          right: 15.0,
                                        ),
                                        child: Text(
                                          "Validade",
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 5.0,
                                          left: 15.0,
                                          right: 15.0,
                                          bottom: 0.0,
                                        ),
                                        child: TextField(
                                          controller:
                                              new TextEditingController(),
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w300,
                                          ),
                                          decoration: inputBorderStyle(),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 10.0,
                                          left: 15.0,
                                          right: 15.0,
                                        ),
                                        child: Text(
                                          "CVC",
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 5.0,
                                          left: 15.0,
                                          right: 15.0,
                                          bottom: 0.0,
                                        ),
                                        child: TextField(
                                          controller:
                                              new TextEditingController(),
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w300,
                                          ),
                                          decoration: inputBorderStyle(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 10.0,
                                          left: 15.0,
                                          right: 15.0,
                                        ),
                                        child: Text(
                                          "Parcelas",
                                          style: TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 120,
                                        margin: EdgeInsets.only(
                                          top: 5.0,
                                          left: 15.0,
                                          right: 15.0,
                                          bottom: 0.0,
                                        ),
                                        child: TextField(
                                          controller:
                                              new TextEditingController(),
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w300,
                                          ),
                                          decoration: inputBorderStyle(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.all(15.0),
                                child: Divider(
                                  color: Color(0xff999999),
                                  height: 1,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    width: 120,
                                    margin: EdgeInsets.only(
                                      left: 15.0,
                                      right: 15.0,
                                    ),
                                    child: Text(
                                      "Total",
                                      style: TextStyle(
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    width: 120,
                                    margin: EdgeInsets.only(
                                      left: 15.0,
                                      right: 15.0,
                                    ),
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Text(
                                        "R\$ 50,00",
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: 5.0,
            ),
            child: Align(
              child: FlatButton(
                padding: EdgeInsets.only(
                  left: 35.0,
                  right: 35.0,
                ),
                color: Color(0xff0000cc),
                child: Text(
                  "Finalizar Compra",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                ),
                onPressed: () {},
              ),
              alignment: Alignment.center,
            ),
          ),
        ],
      ),
    );
  }
}
