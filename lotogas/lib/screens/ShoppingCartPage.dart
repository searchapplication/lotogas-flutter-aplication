import 'package:flutter/material.dart';

class ShoppingCartPage extends StatefulWidget {
  @override
  _ShoppingCartPageState createState() => _ShoppingCartPageState();
}

class _ShoppingCartPageState extends State<ShoppingCartPage> {

  String dropdownValue = "R. Hilda Bergo Duarte, 842";
  final items = ["Gás","Gás Natural","Água Mineral","Água","Galão","Gás","Gás Natural","Água Mineral","Água","Galão"];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body:GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ListView(
          children: <Widget>[
            setHeaderBody(),
            containerTile(height:47.0,child:Text("Produtos",style: TextStyle(fontFamily: 'Roboto-Regular',fontWeight: FontWeight.w400 ,fontSize: 18.0,)),),
            productList(),
            Container(
                height:104.0,
                padding: EdgeInsets.only(left: 16.0,right: 16.0,top: 16,),
                child:Column(
                  children: <Widget>[
                    textPrice("Subtotal","R\$ 175,00","Roboto-Light",FontWeight.w400,Color(0xff999999)),
                    textPrice("Taxa de Entrega","R\$ 5,00","Roboto-Light",FontWeight.w400,Color(0xff999999)),
                    SizedBox(height: 16,),
                    textPrice("Total","R\$ 180,00","'Roboto-Regular'",FontWeight.w400,Color(0xff000000)),
                  ],
                )
            ),



            SizedBox(height: 8.0, child: Container(color:Color.fromRGBO(226, 226, 226, 1)),),
            containerTile(height:47.0,child:Text("Pagamento",style: TextStyle(fontFamily: 'Roboto-Regular',fontWeight: FontWeight.w400 ,fontSize: 18.0,)),),
            containerTile(
              height: 55,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Formas de pagamento",style: TextStyle(fontFamily: 'Roboto-Regular', fontSize: 16.0,)),
                  containerBorder(
                    height: 25,
                    child: materialButton((){}, 4.0,"Escolher", Color(0xff0000cc))
                  ),
                ],
              )
            ),
            Container(
                padding: EdgeInsets.only(left: 16,right: 16,bottom: 17,top: 17),
                alignment: Alignment.centerLeft,
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Observação",style: TextStyle(fontFamily: 'Roboto-Regular', fontSize: 16.0,)),
                      ],
                    ),
                    SizedBox(height: 5,),
                    TextField(
                      maxLines: 3,
                      textAlign: TextAlign.start,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.done,
                      cursorColor: Colors.black,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide:  BorderSide(width: 1, style: BorderStyle.none,color: Color(0xff999999)),),
                        hintText: '(Caso tenha alguma observação, escreva aqui)',
                        contentPadding: EdgeInsets.only(left: 9, bottom: 4, top: 4, right: 9),
                        hintStyle: TextStyle(fontSize: 14,fontWeight: FontWeight.w300,color:Color(0xff999999),fontStyle: FontStyle.normal),
                        focusedBorder:OutlineInputBorder(borderSide: const BorderSide(color: Colors.grey, width: 0.5),),
                      ),
                      style: TextStyle(fontSize: 14,color:Colors.black,decoration: TextDecoration.none,decorationStyle: TextDecorationStyle.solid),
                    ),
                    SizedBox(height: 8,),
                    Center(
                      child: FlatButton(
                        color: Color(0xff0000cc),
                        onPressed: (){
                          print("confirmar Pedido");
                        },
                        child: Text("Confirmar Pedido",style: TextStyle(fontFamily: 'Roboto-Light', fontSize: 16.0,color: Colors.white)),
                      ),
                    ),

                  ],
                )
            ),
          ],
        ),
      ),
    );
  }

  PreferredSize getAppBar(){
    return PreferredSize(
      preferredSize: Size.fromHeight(55),
      child: AppBar(
        elevation: 1.0,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Container(
          child: Text(
            "Compras",
            style: TextStyle(
              fontFamily: 'Roboto-Light',
              fontSize: 16,
              color: Color.fromRGBO(0, 0, 0, 1),
            ),
            textAlign: TextAlign.center,
          ),
        ),
        leading: IconButton(
          alignment: Alignment.center,
          focusColor: Colors.transparent,
          icon: Image.asset(
            "assets/icons/3.0x/icone_voltar.png",
            width: 20,
            height: 20,
          ),
          onPressed: () => {Navigator.pop(context)},
        ),
      ),
    );
  }

  Container containerTile({double height,Widget child}){
    return  Container(
        height:height,
        margin:  EdgeInsets.only(left: 16,right: 16),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Color.fromRGBO(226, 226, 226, 1),))
        ),
        child: child,
    );
  }

  Row textPrice(String title,String value,String fontFamily,FontWeight fontWeight,Color color){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(title,style: TextStyle(fontFamily: fontFamily,fontWeight: fontWeight, fontSize: 16.0, color: color)),
        Text(value,style: TextStyle(fontFamily: fontFamily,fontWeight: fontWeight, fontSize: 16.0,color:color)),
      ],
    );
  }

  Container setHeaderBody(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              top: 16.0,
              bottom: 17.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("ENTREGAR EM", style: TextStyle(fontSize: 16, fontFamily: "Roboto-Regular", color: Color(0xff999999), fontWeight: FontWeight.w400,),),
                    DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        isExpanded: true,
                        isDense: true,
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        iconEnabledColor: Color(0xff0000cc),
                        icon: Icon(Icons.keyboard_arrow_down),
                        items: <String>[
                          "R. Hilda Bergo Duarte, 842",
                          "Av. Marcelino Pires da Silva Costa, 2332",
                          "R. João Damasceno Pires, 1190"
                        ].map<DropdownMenuItem<String>>((String value)  {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value, overflow: TextOverflow.ellipsis, style: TextStyle(fontFamily: "Roboto-Regular", fontSize: 16, fontWeight: FontWeight.w400,),),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
          SizedBox(height: 8.0, child: Container(color:Color.fromRGBO(226, 226, 226, 1),),),
        ],
      ),
    );
  }

  Container setButton(int index){
    return Container(
      margin: EdgeInsets.only(top: 15, bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          containerBorder(
            height: 25,
            child:Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                materialButton((){},5.0,"-",Color(0xffcc0033)),
                Container(
                  color: Colors.transparent,
                  margin: EdgeInsets.only(left: 05, right: 05),
                  child: Text(index.toString(), style: TextStyle(fontSize: 18, fontFamily: "Roboto-Light", color: Colors.black), textAlign: TextAlign.center),
                ),
                materialButton((){},5.0,"+",Color(0xff0000cc)),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 10)),
          Material(
            type: MaterialType.transparency,
            child: InkWell(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              child: containerBorder(
                  height: 25,
                  padding:EdgeInsets.only(right: 5.0, left: 5.0),
                  child:Container(
                    child: Image.asset("assets/icons/3.0x/garbage.png", width: 15),
                  ),
              ),
              onTap: (){
                setState(() {
                  items.removeAt(index);
                });
                print("remover");
              },
            ),
          )
        ],
      ),
    );
  }

  Container containerBorder({double height,EdgeInsetsGeometry padding = const EdgeInsets.only(),Widget child}){
    return Container(
      height: height,
      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), color:Colors.transparent,border: Border.all(color: Color.fromARGB(255, 229, 229, 229), width: 1)),
      padding: padding,
      child: child,
    );
  }

  Column productList(){
    return Column(
      children: items.asMap().map((index,texto)=> MapEntry(index,
        containerTile(
            height:55.0,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(texto,style: TextStyle(fontFamily: 'Roboto-Regular', fontSize: 16.0,)),
                setButton(index)
              ],
            ),
          ),
        ),
      ).values.toList(),
    );
  }

  Material materialButton(Function onTap,double borderRadius,String title,Color color,){
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        splashColor: Colors.black,
        onTap: onTap,
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
        child: Container(
          margin: EdgeInsets.only(left: 05, right: 05),
          child: Text(title, style: TextStyle(fontSize: 18, fontFamily: "Roboto-Light", color: color), textAlign: TextAlign.center),
        ),
      ),
    );
  }
}

