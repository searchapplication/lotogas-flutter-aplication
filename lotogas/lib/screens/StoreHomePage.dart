
import 'package:flutter/material.dart';

class StoreHomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return StoreHomePageState();
  }

}

class StoreHomePageState extends State<StoreHomePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Loja de GÁS GLP I", style: TextStyle(color: Color(0xff020202)),),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xff0000cc)),
        elevation: 0,
        bottom: PreferredSize(child: Container(color: Color.fromARGB(255, 229, 229, 229), height: 1,), preferredSize: Size.fromHeight(1)),
      ),
      body: Column(
        children: <Widget>[
            Expanded(
                child: ListView(
                  children: <Widget>[
                    StoreInformation(),
                    ListView.builder(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: 20,
                        itemBuilder: (BuildContext context, int index) {
                          return ProductItem(isPar: index % 2 == 0);
                        }
                    )
                  ],
                )
              ),
            Container(
              height: 40,
              padding: EdgeInsets.only(right: 16, left: 16),
              color: Color(0xff0000cc),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Image.asset("assets/icons/shopping_cart_5.png", height: 14, width: 16,),
                  ),
                  Positioned.fill(child: Align(alignment: Alignment.center, child: Text("Ver compras", style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Roboto-Light"),),)),
                  Align(
                      alignment: Alignment.centerRight,
                      child: Text("R\$ 175,00", style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: "Roboto-Regular"),)
                  ),
                ],
              ),
            ),
        ]
      ),
    );
  }

}


class StoreInformation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
            child: Text("Informações", style: TextStyle(fontFamily: "Roboto-Regular", fontSize: 18, color: Color(0xff090909)),),
          ),
          Container(
            margin: EdgeInsets.only(right: 16, left: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(width: 96, height: 88, color: Colors.amber),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(left: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(margin: EdgeInsets.only(), child: Text("Loja de Gás GLP I asdf adsf", textAlign: TextAlign.left, style: TextStyle(fontFamily: "Roboto-Regular", fontSize:  16, color: Color(0xff131111)),),),
                        Container(margin: EdgeInsets.only(top: 8), child: Text("Rua Adalberto Celestino, 13", style: TextStyle(fontFamily: "Roboto-Light", fontSize:  14, color: Color(0xff999999)))),
                        Container(
                            margin: EdgeInsets.only(top: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Image.asset("assets/icons/unlocked_1.png", height: 14, width: 10,),
                                    Container(height: 6, width: 6,),
                                    Text("Aberto", style: TextStyle(fontFamily: "Roboto-Light", fontSize:  14, color: Colors.black))
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Image.asset("assets/icons/star_1.png", height: 14, width: 14,),
                                    Container(height: 6, width: 6,),
                                    Text("3,3 (134)", style: TextStyle(fontFamily: "Roboto-Regular", fontSize:  14, color: Color(0xfff2bb18)))
                                  ],
                                )
                              ],
                            )
                        ),
                      ],
                    ),
                  )
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 8),
            child: Text("Melhor Atendimento", style: TextStyle(fontFamily: "Roboto-Regular", fontSize: 16, color: Colors.black),),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Text("Atendemos e entregamos gás e água mineral para\nDourados e Região. ", style: TextStyle(fontFamily: "Roboto-Light", fontSize: 14, color: Colors.black),),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            height: 1,
            color: Color.fromARGB(255, 229, 229, 229),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 16),
            child: Text("Produtos", style: TextStyle(fontFamily: "Roboto-Regular", fontSize: 18, color: Color(0xff090909)),),
          ),
        ],
      ),
    );
  }

}

class ProductItem extends StatelessWidget {

  bool isPar = false;

  ProductItem({this.isPar});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      height: 56,
                      width: 40,
                      color: Colors.cyan,
                      margin: EdgeInsets.only(left: 16, top: 8),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(margin: EdgeInsets.only(top: 8), child: Text("Gás de cozinha GLP", style: TextStyle(fontFamily: "Roboto-Regular", fontSize:  16, color: Colors.black),),),
                          Container(margin: EdgeInsets.only(top: 8), child: Text("R\$ 71,00 cada.", style: TextStyle(fontFamily: "Roboto-Light", fontSize:  14, color: Colors.black))),
                          Container(margin: EdgeInsets.only(top: 8), child: Text("Indisponível", style: TextStyle(fontFamily: "Roboto-Light", fontSize:  14, color: Color(0xff999999)))),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("código: 095342", style: TextStyle(fontFamily: "Roboto-Light", fontSize: 12, color: Color(0xff999999))),
                      Container(
                        height: 21,
                        width: 21,
                        margin: EdgeInsets.only(top: 16),
                        child: Image.asset("assets/icons/share.png"),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 14, bottom: 14),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 36,
                    margin: EdgeInsets.only(left: 16),
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(5)), border: Border.all(color: Color.fromARGB(255, 229, 229, 229), width: 2)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10, right: 05),
                          child: Text("-", style: TextStyle(fontSize: 22, fontFamily: "Roboto-Light", color: Color(0xff0000cc)), textAlign: TextAlign.center),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 05, right: 05),
                          child: Text("0", style: TextStyle(fontSize: 18, fontFamily: "Roboto-Light", color: Colors.black), textAlign: TextAlign.center),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 05, right: 10),
                          child: Text("+", style: TextStyle(fontSize: 22, fontFamily: "Roboto-Light", color: Color(0xff0000cc)), textAlign: TextAlign.center),
                        ),
                      ],
                    ),
                  ),

                  Container(
                    height: 32,
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5)), color: Color(0xff0000cc)),
                    margin: EdgeInsets.only(right: 16),
                    padding: EdgeInsets.only(right: 8, left: 8),
                    alignment: Alignment.center,
                    child: Container(
                        child: Text("Adicionar     R\$ 142,00", style: TextStyle(fontSize: 16, fontFamily: "Roboto-Light", color: Colors.white))
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 1,
              color: Color(0xffe2e2e2),
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 16),
            )
          ],
        )
      );
  }

}