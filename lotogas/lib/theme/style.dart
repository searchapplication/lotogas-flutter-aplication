import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    primaryColor: Colors.blue,
    accentColor: Colors.green,
    hintColor: Colors.black,
    dividerColor: Colors.black,
    buttonColor: Colors.black,
    scaffoldBackgroundColor: Colors.white,
    canvasColor: Colors.white,
  );
}
